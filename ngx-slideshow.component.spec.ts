import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxSlideshowComponent } from './ngx-slideshow.component';

describe('slideshow', () => {
  let component: slideshow;
  let fixture: ComponentFixture<slideshow>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ slideshow ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(slideshow);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
