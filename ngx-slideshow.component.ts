import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';

import { NgxMarketplaceProductsService } from '@4geit/ngx-marketplace-products-service';

@Component({
  selector: 'ngx-slideshow',
  template: require('pug-loader!./ngx-slideshow.component.pug')(),
  styleUrls: ['./ngx-slideshow.component.scss']
})
export class NgxSlideshowComponent implements OnInit, OnDestroy {

  items: any;
  selectedItemIndex = 0;
  private sub: any;

  constructor(
    private productsService: NgxMarketplaceProductsService
  ) { }

  ngOnInit() {
    this.sub = Observable.interval(5000).subscribe(x => {
      this.selectedItemIndex = (this.selectedItemIndex + 1) % this.items.length;
    });
    this.items = this.productsService.items.slice(0, 4);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
