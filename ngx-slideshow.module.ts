import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxSlideshowComponent } from './ngx-slideshow.component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxMaterialModule,
  ],
  declarations: [
    NgxSlideshowComponent
  ],
  exports: [
    NgxSlideshowComponent
  ]
})
export class NgxSlideshowModule { }
